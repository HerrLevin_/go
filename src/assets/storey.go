package assets

import (
	"encoding/json"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"go/src/cors"
	"net/http"
)

type Storey struct {
	Id         uuid.UUID `pg:",pk,notnull,type:uuid,default:uuid_generate_v4()" json:"id"`
	Name       string    `pg:",notnull" json:"name"`
	BuildingId uuid.UUID `pg:",notnull,type:uuid" json:"building_id"`
	Building   *Building `pg:"rel:has-one" json:"building,omitempty"`
}

func HandleStorey(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		muxVars := mux.Vars(r)
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "GET" {
			storey := &Storey{Id: uuid.FromStringOrNil(muxVars["id"])}
			err := db.Model(storey).WherePK().Select()
			if err != nil {
				panic(err)
			}
			b, err := json.Marshal(&storey)
			fmt.Fprintf(w, string(b))
		} else {
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}

func HandleStoreys(db *pg.DB) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		urlParams := r.URL.Query()
		buildingId := uuid.Nil
		if len(urlParams["building_id"]) > 0 {
			buildingId = uuid.FromStringOrNil(urlParams["building_id"][0])
		}
		cors.AddHeaders(w, r)
		w.Header().Set("Content-Type", "application/json")
		switch r.Method {
		case "GET":
			handleStoreysGet(w, r, db, buildingId)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}

func handleStoreysGet(w http.ResponseWriter, r *http.Request, db *pg.DB, buildingId uuid.UUID) {
	var storeys []Storey
	var err error
	if buildingId != uuid.Nil {
		err = db.Model(&storeys).Where("building_id = ?", buildingId.String()).Select()
	} else {
		err = db.Model(&storeys).Select()
	}
	if err != nil {
		panic(err)
	}
	b, err := json.Marshal(&storeys)
	fmt.Fprintf(w, string(b))
}
