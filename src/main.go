package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/gorilla/mux"
	"go/src/assets"
	"go/src/cors"
	"go/src/reservations"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"

	jwt "github.com/golang-jwt/jwt"
)

type Realm struct {
	PublicKey string `json:"public_key"`
}

func isAuthorized() http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cors.AddHeaders(w, r)

		if r.Header["Authorization"] != nil {
			var host = os.Getenv("KEYCLOAK_HOST")
			var realmName = os.Getenv("KEYCLOAK_REALM")
			var url = "http://" + host + "/auth/realms/" + realmName

			req, err := http.NewRequest("GET", url, nil)
			if err != nil {
				log.Fatalln(err)
			}
			var jaegerTracecontextheadername = "uber-trace-id"
			val, ok := os.LookupEnv("JAEGER_TRACECONTEXTHEADERNAME")
			if ok {
				jaegerTracecontextheadername = val
			}
			if r.Header[jaegerTracecontextheadername] != nil {
				req.Header.Set(jaegerTracecontextheadername, r.Header.Get(jaegerTracecontextheadername))
			}
			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				log.Fatalln(err)
			}

			defer resp.Body.Close()
			bodyBytes, _ := ioutil.ReadAll(resp.Body)

			var realm Realm
			err = json.Unmarshal(bodyBytes, &realm)
			if err != nil {
				log.Fatalln(err)
			}
			SecretKey := "-----BEGIN CERTIFICATE-----\n" + realm.PublicKey + "\n-----END CERTIFICATE-----"

			mySigningKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(SecretKey))
			if err != nil {
				fmt.Errorf("There was an error parsing pubkey")
				return
			}
			var plainHeader = r.Header["Authorization"][0]
			re := regexp.MustCompile(`[Bb]earer `)
			var reqToken = re.ReplaceAllString(plainHeader, "")

			token, err := jwt.Parse(reqToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
					return nil, fmt.Errorf("There was an error")
				}
				return mySigningKey, nil
			})

			if err != nil {
				fmt.Fprintf(w, err.Error())
			}

			if token.Valid {
				b, err := json.Marshal(token)
				w.Header().Set("Content-Type", "application/json")
				fmt.Fprintf(w, string(b))
				if err != nil {
					fmt.Println(err)
					return
				}
			} else {
				fmt.Fprintf(w, "Invalid Token")
			}
		} else {

			fmt.Fprintf(w, "Not Authorized")
		}
	})
}

func handleRequests(assetsDb *pg.DB, reservationsDb *pg.DB) {
	r := mux.NewRouter()
	if assetsDb != nil {
		r.Handle("/assets/buildings/", assets.HandleBuildings(assetsDb))
		r.Handle("/assets/buildings/{id:[0-9a-fA-F-]+}/", assets.HandleBuilding(assetsDb))
		r.Handle("/assets/storeys/", assets.HandleStoreys(assetsDb))
		r.Handle("/assets/storeys/{id:[0-9a-fA-F-]+}/", assets.HandleStorey(assetsDb))
		r.Handle("/assets/rooms/", assets.HandleRooms(assetsDb))
		r.Handle("/assets/rooms/{id:[0-9a-fA-F-]+}/", assets.HandleRoom(assetsDb))
	}
	if reservationsDb != nil {
		r.Handle("/reservations/", reservations.HandleReservations(reservationsDb))
		r.Handle("/reservations/{id:[0-9a-fA-F-]+}/", reservations.HandleReservation(reservationsDb))
	}
	r.Handle("/jwt/", isAuthorized())
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(":9000", nil))
}

func main() {
	var assetsDb *pg.DB = nil
	var reservationsDb *pg.DB = nil
	{
		val, ok := os.LookupEnv("POSTGRES_ASSETS_DBNAME")
		if ok {
			assetsDb = pg.Connect(&pg.Options{
				User:     os.Getenv("POSTGRES_ASSETS_USER"),
				Password: os.Getenv("POSTGRES_ASSETS_PASSWORD"),
				Database: val,
				Addr:     fmt.Sprintf("%s:%s", os.Getenv("POSTGRES_ASSETS_HOST"), os.Getenv("POSTGRES_ASSETS_PORT")),
			})
			defer assetsDb.Close()

			err := assets.CreateSchema(assetsDb)
			if err != nil {
				panic(err)
			}
		}
	}
	{
		val, ok := os.LookupEnv("POSTGRES_RESERVATIONS_DBNAME")
		if ok {
			reservationsDb = pg.Connect(&pg.Options{
				User:     os.Getenv("POSTGRES_RESERVATIONS_USER"),
				Password: os.Getenv("POSTGRES_RESERVATIONS_PASSWORD"),
				Database: val,
				Addr:     fmt.Sprintf("%s:%s", os.Getenv("POSTGRES_RESERVATIONS_HOST"), os.Getenv("POSTGRES_RESERVATIONS_PORT")),
			})
			defer reservationsDb.Close()

			err := reservations.CreateSchema(reservationsDb)
			if err != nil {
				panic(err)
			}
		}
	}
	handleRequests(assetsDb, reservationsDb)
}
